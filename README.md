# id-management


Here you can find a blockchain Application for identity management. 
The blockchain part is build with Hyperledger. The frondend uses the Vue.js 
framework. 


To test the installation, start HL Fabric, generate a businessnetwork
and deploy this to the running blockchain network

List composer wallet to check available business network cards
composer card list
Start Hyperledger Fabric network and create a Peer Admin card (if it does not already
exist)
~/fabric-dev-servers/startFabric.sh
~/fabric-dev-servers/createPeerAdminCard.sh
cd ~/Development/blockchain
Generate a business network (answer questions in the wizard)
yo hyperledger-composer:businessnetwork
cd <created-project-dir>
npm install

Install Hyperledger Composer...in this example <created-project-dir> is test-network
composer network install -c PeerAdmin@hlfv1 -a ./dist/test-network.bna

Start the business network
composer network start -c PeerAdmin@hlfv1 -n test-network -V 0.0.1 -A admin -S adminpw -f
delete-me.card

Import admin card for businessnetwork
composer card import -f delete-me.card
rm delete-me.card

Ping the network
composer network ping -c admin@test-network

Start Composer Playground and generate a REST server

Start playground
composer-playground &

generate REST server (in this case admin card is from test-network)
composer-rest-server -c admin@test-network -n never -w true &


Explain how to teardown / stop Fabric
Contains some basic Docker commands to check whether Hyperledger Fabric is
running and how to clean up a stopped network….

Stop Hyperledger Fabric
~/fabric-dev-servers/stopFabric.sh
List all docker containers (running and stopped)
docker ps -a
Remove all stopped containers
docker rm $(docker ps -qa)
Remove docker images containing business networks
docker rmi $(docker images dev-peer* -q)