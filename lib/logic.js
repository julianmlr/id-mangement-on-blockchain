'use strict';

/**
 * Request Data transaction
 * @param {com.group4.idmanager.RequestDataTransaction} requestDataTransaction
 * @transaction
 */
async function requestDataTransaction(tx) {
    const factory = getFactory();
    const id = Date.now().toString().substring(10, 13);
    let asset = factory.newResource('com.group4.idmanager', 'RequestedDataAsset', '' + id );

    asset.owner = tx.owner;
    asset.viewer = tx.viewer;
    asset.requestedFields = tx.requestedFields;

    const assetRegistry = await getAssetRegistry('com.group4.idmanager.RequestedDataAsset');

    await assetRegistry.add(asset);
    
    // maybe event here
}

/**
 * Allow Request transaction
 * @param {com.group4.idmanager.AllowRequestTransaction} allowRequestTransaction
 * @transaction
 */
async function allowRequestTransaction(tx) {
    if (tx.granted === true) {
      const factory = getFactory();
      const id = Date.now().toString().substring(10, 13);
      let asset = factory.newResource('com.group4.idmanager', 'SharedPersonalDataAsset', '' + id );
      
      tx.request.requestedFields.forEach(str => asset[str] = tx.request.owner[str]);
      
      asset.owner = tx.request.owner;
      asset.viewer = tx.request.viewer;
      
      const assetRegistry = await getAssetRegistry('com.group4.idmanager.SharedPersonalDataAsset');
      await assetRegistry.add(asset);
    } else {
      // Do nothing
    }
    
    const assetRequestedDataRegistry = await getAssetRegistry('com.group4.idmanager.RequestedDataAsset');
    assetRequestedDataRegistry.remove(tx.request);
    // maybe event here
}

/**
 * Allow Request transaction
 * @param {com.group4.idmanager.RevokeAccessTransaction} revokeAccessTransaction
 * @transaction
 */
async function revokeAccessTransaction(tx) {
    const asset = await getAssetRegistry('com.group4.idmanager.SharedPersonalDataAsset');
    asset.remove(tx.sharedPersonalDataAsset);
    // maybe event here
}
