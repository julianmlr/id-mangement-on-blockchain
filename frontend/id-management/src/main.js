import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Vuetify from 'vuetify'
import Router from 'vue-router'

import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

// Components
import SignIn from '@/components/SignIn'
import SignUp from '@/components/SignUp'
import Request from '@/components/Request'
import SeeRequests from '@/components/SeeRequests'


Vue.use(Router)

let router = new Router({
  routes: [{
      path: '/signin',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/request',
      name: 'Request',
      component: Request
    },
    {
      path: '/SeeRequests',
      name: 'SeeRequests',
      component: SeeRequests
    },


  ]
})

// index.js or main.js
import 'vuetify/dist/vuetify.min.css'
// index.js or main.js
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader

// import Vue from 'vue'
// import VueRouter from 'vue-router'

// Vue.use(VueRouter)
Vue.use(Vuetify)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')