import Vue from 'vue'
import Router from 'vue-router'

// Components
import SignIn from '@/components/SignIn'
import SignUp from '@/components/SignUp'
import LogIn from '@/components/LogIn'
import SeeRequests from '@/components/SeeRequests'



Vue.use(Router)

let router = new Router({
  routes: [{
      path: '/',
      redirect: '/signin'
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/login',
      name: 'LogIn',
      component: LogIn
    },
    {
      path: '/see-request',
      name: 'SeeRequests',
      component: SeeRequests
    },

  ]
})


export default router